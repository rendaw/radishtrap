# Radish Trap

A game about a triangle finding the highest number while avoiding sea life.

Mouse/touch only.  Click once to start moving.  Finding higher numbers quickly gives you more points.  Aim for the highest number!

Play at [https://rendaw.gitlab.io/radishtrap](https://rendaw.gitlab.io/radishtrap)!