#!/usr/bin/env python
import os
import os.path
import subprocess
import shutil

subprocess.check_call(['npm', 'install'])
shutil.rmtree('dist', ignore_errors=True)
source = 'source'
subprocess.check_call([
    './node_modules/.bin/parcel', 'build',
    '--no-source-maps',
    '--public-url', '.',
    '{}/index.html'.format(source),
])
for extra in os.listdir(source):
    if extra.endswith((
        '.ts',
        '.html',
    )):
        continue
    shutil.copy('{}/{}'.format(source, extra), 'dist')
