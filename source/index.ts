import "babel-polyfill";
import * as pixi from "pixi.js";

import { myWs, AsyncWebsockets } from "./ws";
import { Layer } from "./layer";
import {
  app,
  key_enter,
  key_escape,
  set_do_game_over,
} from "./globals";
import {
  rgb2num,
  createText,
  createHeadText,
  createNoteText
} from "./pixihelp";
import { setGuppyTexture } from "./entityguppy";
import { setMinnowTexture } from "./entityminnow";
import { setJellyTextures } from "./entityjellies";
import { setStarTextures } from "./entitystar";
import { DemoLayer } from "./layerdemo";
import { GameLayer } from "./layergame";

// Generate initial room

const diffNames = ["normal", "kind of hard"];

const createButtons = (
  left: [string, () => void],
  right: [string, () => void]
) => {
  const createInner = (b: [string, () => void]) => {
    const cont = new pixi.Container();
    const text = createText(b[0], 28);
    text.position.set(
      Math.floor(app.renderer.width / 4 - text.width / 2),
      Math.floor(50 - text.height / 2)
    );
    cont.addChild(text);
    const bg = new pixi.Graphics();
    cont.addChild(bg);
    cont.interactive = true;
    const drawBg = () => {
      bg.moveTo(0, 0);
      bg.lineTo(app.renderer.width / 2, 0);
      bg.lineTo(app.renderer.width / 2, 100);
      bg.lineTo(0, 100);
      bg.closePath();
    };
    cont.on("pointerover", () => {
      bg.clear();
      bg.beginFill(pixi.utils.rgb2hex([1, 1, 1]), 0.2);
      drawBg();
      bg.endFill();
    });
    cont.on("pointerout", () => {
      bg.clear();
      bg.beginFill(pixi.utils.rgb2hex([1, 1, 1]), 0);
      drawBg();
      bg.endFill();
    });
    cont.on("pointerdown", () => {
      bg.clear();
      bg.beginFill(pixi.utils.rgb2hex([1, 1, 1]), 0);
      drawBg();
      bg.endFill();
    });
    cont.on("pointerup", () => b[1]());
    bg.beginFill(pixi.utils.rgb2hex([1, 1, 1]), 0);
    drawBg();
    bg.endFill();
    return cont;
  };
  const cont = new pixi.Container();
  cont.position.y = app.renderer.height - 100;
  if (left) {
    const b = createInner(left);
    cont.addChild(b);
  }
  if (right) {
    const b = createInner(right);
    b.position.x = app.renderer.width / 2;
    cont.addChild(b);
  }
  const bg = new pixi.Graphics();
  bg.lineStyle(2, pixi.utils.rgb2hex([1, 1, 1]), 1);
  bg.moveTo(app.renderer.width / 2, 10);
  bg.lineTo(app.renderer.width / 2, 90);
  cont.addChild(bg);
  return cont;
};

class TitleLayer extends Layer {
  constructor(bg: Layer) {
    super();
    const title = createHeadText("RADISH TRAP");
    title.position.set(Math.min(50, title.position.x), 40);
    this.graphics.addChild(title);
    const buttons = createButtons(
      [
        diffNames[1],
        () => {
          removeLayer(this);
          addLayer(new GoLayer(1, bg));
        }
      ],
      [
        diffNames[0],
        () => {
          removeLayer(this);
          addLayer(new GoLayer(0, bg));
        }
      ]
    );
    this.graphics.addChild(buttons);
  }
}

class GoLayer extends Layer {
  bg: Layer;
  diff: number;
  constructor(diff: number, bg: Layer) {
    super();
    this.diff = diff;
    this.bg = bg;
    const title = createHeadText("ARE YOU READY!");
    title.position.set(Math.min(50, title.position.x), 50);
    this.graphics.addChild(title);
    this.graphics.addChild(
      createButtons(["NO", () => this.back()], ["GO", () => this.go()])
    );
  }
  back() {
    removeLayer(this);
    addLayer(new TitleLayer(this.bg));
  }
  go() {
    removeLayer(this.bg);
    removeLayer(this);
    addLayer(new GameLayer(this.diff));
  }
  update(delta: number) {
    if (key_enter()) this.go();
    else if (key_escape()) this.back();
  }
}

class GameOverLayer extends Layer {
  diff: number;
  bg: GameLayer;
  constructor(bg: GameLayer, diff: number, score: number) {
    super();
    this.bg = bg;
    this.diff = diff;

    const diffLabel = createText(diffNames[diff], 28);
    diffLabel.position.set(app.renderer.width / 2 - diffLabel.width / 2, 10);
    this.graphics.addChild(diffLabel);

    const scoreBox = new pixi.Container();
    this.graphics.addChild(scoreBox);
    const title = createHeadText("THE END");
    title.position.set(0, 0);
    scoreBox.addChild(title);
    const scoreLabel = createText("Score:", 28);
    scoreLabel.position.set(0, 70);
    scoreBox.addChild(scoreLabel);
    const scoreText = createText("" + Math.floor(score), 70);
    scoreText.position.set(100, 70);
    scoreBox.addChild(scoreText);
    scoreBox.position.set(
      app.renderer.width / 2 - scoreBox.width / 2,
      app.renderer.height / 2 - scoreBox.height / 2
    );

    const buttons = createButtons(
      ["title", () => this.back()],
      ["retry", () => this.go()]
    );
    this.graphics.addChild(buttons);
  }

  back() {
    removeLayer(this.bg);
    removeLayer(this);
    const demoLayer = new DemoLayer();
    addLayer(demoLayer);
    addLayer(new TitleLayer(demoLayer));
  }

  go() {
    removeLayer(this.bg);
    removeLayer(this);
    const demoLayer = new DemoLayer();
    addLayer(demoLayer);
    addLayer(new GoLayer(this.diff, demoLayer));
  }

  update(delta: number) {
    if (key_enter()) this.go();
    else if (key_escape()) this.back();
  }
}

const layers = [];

const addLayer = (l: Layer) => {
  app.stage.addChild(l.graphics);
  layers.push(l);
};

const removeLayer = (l: Layer) => {
  app.stage.removeChild(l.graphics);
  layers.splice(layers.indexOf(l), 1);
};

set_do_game_over((context: any, diff: number, score: number) => {
  addLayer(new GameOverLayer(<GameLayer>context, diff, score));
});

const main = () => {
  setGuppyTexture(PIXI.Texture.fromFrame("guppy"));
  setMinnowTexture(PIXI.Texture.fromFrame("minnow"));
  setStarTextures(
    PIXI.Texture.fromFrame("stara"),
    PIXI.Texture.fromFrame("starb")
  );
  const bigJellyFrames = [];
  for (let i = 0; i < 8; ++i) {
    bigJellyFrames.push(PIXI.Texture.fromFrame("bigjelly000" + i));
  }
  setJellyTextures(
    PIXI.Texture.fromFrame("smallleg"),
    PIXI.Texture.fromFrame("jelly"),
    PIXI.Texture.fromFrame("bigleg"),
    bigJellyFrames
  );

  const demoLayer = new DemoLayer();
  addLayer(demoLayer);
  addLayer(new TitleLayer(demoLayer));

  app.ticker.add((_: number) => {
    const delta = Math.min(0.1, app.ticker.elapsedMS / 1000);
    for (let layer of layers) layer.update(delta);
    //console.log(vpool.poolfree.length);
  });
};

PIXI.loader.add("sprites.json").load(main);
